Welcome to Discount Mini Storage of The Villages in Lady Lake, FL! We have a variety of Climate and Non-Climate Controlled unit sizes to achieve your storage solution. Our on-site management team takes pride in personal service and maintaining an extremely clean, secure and pest free environment.

Address: 708 S Hwy 441, Lady Lake, FL 32159

Phone: 352-259-8128
